<!DOCTYPE html>
<html>
<head>
<title>Registro</title>
<meta charset="utf-8">
 <link rel="stylesheet" type="text/css" href="css/registro.css">
 <!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/7.8.0/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.8.0/firebase-analytics.js"></script>
<script>
 // Your web app's Firebase configuration
 var firebaseConfig = {
 apiKey: "AIzaSyAubf4H_XNcCOzglyc1J2yL6iJWEzFdHmY",
 authDomain: "happysmile-6e877-8de5c.firebaseapp.com",
 databaseURL: "https://happysmile-6e877-8de5c.firebaseio.com",
 projectId: "happysmile-6e877-8de5c",
 storageBucket: "happysmile-6e877-8de5c.appspot.com",
 messagingSenderId: "366733427898",
 appId: "1:366733427898:web:dff2cbdf1ec9ca00fd2e83",
 measurementId: "G-CW1VGGT3BC"
 };
 // Initialize Firebase
 firebase.initializeApp(firebaseConfig);
 firebase.analytics();
</script>
 var database = firebase.database();
 window.addEventListener('load', function(){

 let nombre= document.querySelector('#nombre');
 let usuario= document.querySelector('#usuario');
 let universidad= document.querySelector('#universidad');
 let titulo= document.querySelector('#titulo');
 let especialidad= document.querySelector('#especialidad');
 let correo= document.querySelector('#correo');
 let contrasena= document.querySelector('#contrasena');
 let contrasena2= document.querySelector('#contrasena2');
 let guardar= document.querySelector('#guardar');
 let consultar= document.querySelector('#btnconsultar');
 let listado= document.querySelector('#listado');
 guardar.addEventListener('click',function(){
 firebase.database().ref('usuarios/'+ usuario.value ).set({
 nombre: nombre.value,
 usuario: usuario.value,
 universidad: universidad.value,
 titulo: titulo.value,
 especialidad: especialidad.value,
 correo: correo.value,
 contrasena: contrasena.value


 })
})
});

 </script>
</head>
<body>
<header>
 <div class="contenedor">
 <h1 class="icon-mask">Happy Smile</h1>
 <input type="checkbox" id="menu-bar">
 <label class="fontawesome-align-justify" for="menu-bar"></label>
 <nav class="menu">
<a href="index.html">Inicio</a>
 <a href="registro.html">Registro</a>
 </nav>
 </div>

 </header>
 <section id="banner">
 </section>
 <form action="" >
 <h2>INICIE EL REGISTRO</h2>
 <label>Nombres Completos</label>
 <input type="text" id="nombre" >
 <label>Nombre de Usuario</label>
 <input type="text" id="usuario" >
 <label>Universidad a la que asistio</label>
 <input type="text" id="universidad" >
 <label>Titulo Universitario</label>
 <input type="text" id="titulo" >
 <label>Especialidad</label>
 <input type="text" id="especialidad" >
 <label>Correo electronico</label>
 <input type="text" id="correo" >
 <label>Contraseña</label>
 <input type="txt" id="contrasena" >
 <label>Repita la contraseña</label>
 <input type="txt" id="contrasena2">
 <button id="guardar">Registrar</button>
 <div id="listado"></div>
</body>
</html>

Diseñar una página web de ingreso de datos de clientes, utilizando HTML, CSS y Javascript. Adjuntar el codigo y la ejecución en un archivo .pdf
